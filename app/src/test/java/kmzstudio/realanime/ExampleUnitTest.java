package kmzstudio.realanime;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void f(int n, float x) throws Exception {
        System.out.println("f(int n, float x) n="+ n +" x="+x);
        //assertEquals(4, 2 + 2);
    }
    private void f(long q, double y) throws Exception {
        System.out.println("f(long q, double y) q="+ q +" y="+y);
        //assertEquals(4, 2 + 2);
    }
    public void f(double y1, double y2) throws Exception {
        System.out.println("f(double y1, double y2) y1="+ y1 +" y2="+y2);
        //assertEquals(4, 2 + 2);
    }

    public void g() throws Exception {
        int n=1; long q=12; float x=1.5f; double y =2.5;
        System.out.println("---- dasn g");
        f(n, q);
        f(q, n);
        f(n, x);
        f(n, y);
        //assertEquals(4, 2 + 2);
    }
}

class SurfAcc {
    public static void main (String args[]) throws Exception {
        ExampleUnitTest a = new ExampleUnitTest();
        a.g();
    }
}