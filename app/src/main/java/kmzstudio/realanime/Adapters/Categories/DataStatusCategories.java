package kmzstudio.realanime.Adapters.Categories;

import java.util.List;

import kmzstudio.realanime.Model.Categories;
import kmzstudio.realanime.Post;

/**
 * Created by Amine Karimi on 04/10/2017.
 */

public interface DataStatusCategories {
    void onSuccess(List<Categories> Categories);
    void onError(Exception e);
}
