package kmzstudio.realanime.Adapters.Categories;

import android.view.View;

import kmzstudio.realanime.Model.Categories;
import kmzstudio.realanime.Post;


/**
 * Created by yousra on 04/10/2017.
 */

public interface RecyclerViewClickListenerCategories {
    public void onClick(View view, Categories categories);
}
