package kmzstudio.realanime.Adapters.Categories;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import kmzstudio.realanime.Model.Categories;
import kmzstudio.realanime.R;


public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private List<Categories> categories = new ArrayList<>();
    private Context context;
    private RecyclerViewClickListenerCategories mClickListener;

    public CategoriesAdapter(Context context, RecyclerViewClickListenerCategories clickListener) {
        this.context = context;
        this.mClickListener = clickListener;
    }

    public void refresh(ArrayList<Categories> newList) {
        this.categories.clear();
        this.categories = newList;
    }


    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home,
                parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindModel(categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Categories mCategorie;
        ImageView category_background;
        TextView category_title;


        ViewHolder(View v) {
            super(v);
            category_background = itemView.findViewById(R.id.category_background);
            category_title = itemView.findViewById(R.id.category_title);

            v.setOnClickListener(this);
        }

        @SuppressLint("SetTextI18n")
        void bindModel(Categories categorie) {

            this.mCategorie = categorie;

            category_title.setText(mCategorie.getTitle());

            Glide.with(context)
                    .load(mCategorie.getImage())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .into(category_background);


        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, mCategorie);
        }
    }

}
