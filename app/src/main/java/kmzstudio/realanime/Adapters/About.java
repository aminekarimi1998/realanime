package kmzstudio.realanime.Adapters;

import android.content.res.Configuration;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatDelegate;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;
import java.util.Calendar;

import kmzstudio.realanime.BuildConfig;
import kmzstudio.realanime.R;
import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        simulateDayNight(/* DAY */ 0);
        Element versionElement = new Element();
        versionElement.setTitle(BuildConfig.VERSION_NAME);

        String desc = "KMZ is a combination of names (Karimi, Mouad, Zineb) of developers who develop the applications"
                +" in this play account. We are computer science students we begin this by curiousity.";

        Element artCopyrights = new Element();
        artCopyrights.setTitle("There is no copyright infringement intended for the wallpapers. " +
                " If you have an issue with me sharing this wallpapers please contact me through mail ." +
                " Once I have received your message and determined you are the proper owner of this content" +
                " I will have it removed, no drama at all, and thanks ");

        Element ads = new Element();
        ads.setTitle("I want to apologize about the ads but it is too necessary for us to continue in this domain, " +
                " we put all our energy to create new features and the app is totally free, so the ads is our unic source.");

        View aboutPage = new AboutPage(this)
                .isRTL(false)
                .setImage(R.drawable.kmzbg)
                .setDescription(desc)
                .addItem(artCopyrights)
                .addItem(ads)
                .addGroup("Connect with us")
                .addEmail("realanimeartworks@gmail.com")
                .addPlayStore("kmzstudio.realanime")
                .addFacebook("hackira0")
                .addInstagram("amine_karimii")
                .addItem(versionElement)
                .create();
        setContentView(aboutPage);
    }

    Element getCopyRightsElement() {
        Element copyRightsElement = new Element();
        final String copyrights = String.format(getString(R.string.copy_right), Calendar.getInstance().get(Calendar.YEAR));
        copyRightsElement.setTitle(copyrights);
        copyRightsElement.setIconTint(mehdi.sakout.aboutpage.R.color.about_item_icon_color);
        copyRightsElement.setIconNightTint(android.R.color.white);
        copyRightsElement.setGravity(Gravity.CENTER);
        copyRightsElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(About.this, copyrights, Toast.LENGTH_SHORT).show();
            }
        });
        return copyRightsElement;
    }

    void simulateDayNight(int currentSetting) {
        final int DAY = 0;
        final int NIGHT = 1;
        final int FOLLOW_SYSTEM = 3;

        int currentNightMode = getResources().getConfiguration().uiMode
                & Configuration.UI_MODE_NIGHT_MASK;
        if (currentSetting == DAY && currentNightMode != Configuration.UI_MODE_NIGHT_NO) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_NO);
        } else if (currentSetting == NIGHT && currentNightMode != Configuration.UI_MODE_NIGHT_YES) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_YES);
        } else if (currentSetting == FOLLOW_SYSTEM) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        }
    }
}
