package kmzstudio.realanime.Adapters;

import android.view.View;

import kmzstudio.realanime.Model.Categories;
import kmzstudio.realanime.Post;


/**
 * Created by yousra on 04/10/2017.
 */

public interface RecyclerViewClickListener {
    public void onClick(View view, Post post);
}
