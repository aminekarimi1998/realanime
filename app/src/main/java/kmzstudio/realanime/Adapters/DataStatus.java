package kmzstudio.realanime.Adapters;

import kmzstudio.realanime.Model.Categories;
import kmzstudio.realanime.Post;

import java.util.List;

/**
 * Created by Amine Karimi on 04/10/2017.
 */

public interface DataStatus {
    void onSuccess(List<Post> posts);
    void onError(Exception e);
}
