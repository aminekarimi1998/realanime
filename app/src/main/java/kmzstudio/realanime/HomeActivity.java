package kmzstudio.realanime;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kmzstudio.realanime.Adapters.Categories.CategoriesAdapter;
import kmzstudio.realanime.Artist.ArtistAdapter;
import kmzstudio.realanime.Artist.ArtistDataStatus;
import kmzstudio.realanime.Artist.ArtistPost;
import kmzstudio.realanime.Artist.ListArtistWalls;

import static kmzstudio.realanime.Constants.categories;

public class HomeActivity extends AppCompatActivity {

    private ArtistAdapter adapter;
    private DiscreteScrollView artistRecycler;
    private View splash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        TextView title = findViewById(R.id.title1);
        title.setText(R.string.toolbar_home_title);

        refreshContent();

        ImageButton favorite = findViewById(R.id.favorit);
        favorite.setVisibility(View.VISIBLE);
        favorite.setOnClickListener(view ->
                startActivity(new Intent(HomeActivity.this, FavoriteActivity.class)));

        splash = findViewById(R.id.splash_screen_home);
        artistRecycler = findViewById(R.id.discreteScrollView);
        artistRecycler.setItemTransformer(new ScaleTransformer.Builder()
                .setMaxScale(1.05f)
                .setMinScale(0.8f)
                .setPivotX(Pivot.X.CENTER)
                .build());
        RecyclerView categories_recycler = findViewById(R.id.rc_home_categories);

        CategoriesAdapter adapter =
                new CategoriesAdapter(this, (view, category)
                        -> intentStart(category.getTitle()
                        .toLowerCase().replace(" ", "")));

        adapter.refresh(categories);

        categories_recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL, false));

        categories_recycler.setAdapter(adapter);

    }

    void intentStart(String cat) {
        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
        intent.putExtra("selectedCat", cat);
        startActivity(intent);
    }

    private void refreshContent() {

        fetchRemoteData(new ArtistDataStatus() {
            @Override
            public void onSuccess(List<ArtistPost> posts) {
                splash.setVisibility(View.GONE);
                if (adapter == null) {
                    adapter = new ArtistAdapter(posts, HomeActivity.this, (view, post) -> {
                        Intent intent = new Intent(HomeActivity.this, ListArtistWalls.class);
                        intent.putExtra("artistName", post.getProfilName());
                        startActivity(intent);
                    });
                    artistRecycler.setAdapter(adapter);

                } else {
                    adapter.getItems().clear();
                    adapter.getItems().addAll(posts);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(HomeActivity.this, "There is an error, please try again.", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void fetchRemoteData(final ArtistDataStatus callback) {

        String queryUrl = BuildConfig.li + "artists";

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                queryUrl,
                s -> {

                    List<ArtistPost> listItems = new ArrayList<>();
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        JSONArray array = jsonObject.getJSONArray("bgs");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);

                            ArtistPost item = new ArtistPost(
                                    object.getString("nameArtist"),
                                    object.getString("profilePic"),
                                    object.getString("facebook"),
                                    object.getString("instagram")
                            );

                            listItems.add(item);

                        }
                        callback.onSuccess(listItems);

                    } catch (JSONException e) {
                        // to custom
                        // callback.onError(e);
                    }
                },
                error -> {
                    Toast.makeText(HomeActivity.this, "There is an error please try later !", Toast.LENGTH_LONG).show();
                    //loading.setVisibility(View.INVISIBLE);
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

}
