package kmzstudio.realanime;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FavoriteActivity extends AppCompatActivity {

    RecyclerView.LayoutManager layoutManager;
    List<Post> listItems;
    TextView favtext, title1;
    ImageView favImage;
    ArrayList<Post> myfavSaved;
    private RecyclerView recyclerView;
    private MyAdapter adapter;

    public FavoriteActivity() {
        // Required empty public constructor
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorito_senpai);

        recyclerView = findViewById(R.id.rc);
        layoutManager = new GridLayoutManager(FavoriteActivity.this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        favtext = findViewById(R.id.favtext);
        favImage = findViewById(R.id.imageView4);
        title1 = findViewById(R.id.title1);
        //logoCategorie = findViewById(R.id.logoCategorie);

        title1.setText("Favorite");

        fetchRemoteData();
    }

    private void fetchRemoteData() {
        TinyDB tinydb = new TinyDB(FavoriteActivity.this);
        listItems = new ArrayList<>();
        myfavSaved = tinydb.getListObject("fav", Post.class);
        for (int i = 0; i < myfavSaved.size(); i++) {
            Post item = new Post(myfavSaved.get(i).getImgUrl(),
                    myfavSaved.get(i).getCat_cover_medium(),
                    myfavSaved.get(i).getPremium(),
                    myfavSaved.get(i).getFacebook(),
                    myfavSaved.get(i).getInstagram(),
                    myfavSaved.get(i).getName()
            );
            listItems.add(item);
        }
        if (listItems.size() != 0) {
            favtext.setVisibility(View.GONE);
            favImage.setVisibility(View.GONE);
        }
        Collections.reverse(listItems);
        onSuccess(listItems);

    }

    public void onSuccess(List<Post> posts) {
        if (adapter == null) {
            adapter = new MyAdapter(posts, FavoriteActivity.this, (view, post) -> {
                Intent intent = new Intent(FavoriteActivity.this, SetFullWallpaper.class);
                intent.putExtra("img", post.getImgUrl());
                intent.putExtra("cover", post.getCat_cover_medium());
                intent.putExtra("artistName", post.getName());
                intent.putExtra("premium", post.getPremium());
                intent.putExtra("facebook", post.getFacebook());
                intent.putExtra("ig", post.getInstagram());
                startActivity(intent);
            });
            recyclerView.setAdapter(adapter);


        } else {
            adapter.getItems().clear();
            adapter.getItems().addAll(posts);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchRemoteData();
        if (listItems.size() == 0) {
            favtext.setVisibility(View.VISIBLE);
            favImage.setVisibility(View.VISIBLE);
        }
    }
}
