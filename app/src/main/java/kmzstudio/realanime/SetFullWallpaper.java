package kmzstudio.realanime;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.WallpaperInfo;
import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import jp.wasabeef.blurry.Blurry;
import kmzstudio.realanime.Parallax.LiveWallpaperService;
import mehdi.sakout.fancybuttons.FancyButton;

public class SetFullWallpaper extends AppCompatActivity implements RewardedVideoAdListener {
    //private ProgressDialog waitToSetDialog;
    public static final int CODE_PERMISSION_SET = 1;
    public static final int PERMISSION_REQUEST_CODE_FOR_CROP = 2;
    public static final int CODE_PERMISSION_SHARE = 3;
    public static final int SET_LIVE_WALL_REQUEST = 553;
    public static final int CODE_PERMISSION_SAVE = 4;
    final AdRequest adRequest = new AdRequest.Builder().build();
    RewardedVideoAd mRewardedVideoAd;
    InterstitialAd mInterstitialAd;
    SensorManager sensorManager;
    private Sensor sensor;
    private SharedPreferences.Editor editor;
    SharedPreferences preferences;
    ImageButton fav;
    TinyDB tinydb;
    String url, name, fb, ig, thumb;
    boolean premium;
    private FancyButton setWallpaper, Download, share, unlock;
    private boolean clicked;
    private int a;
    private ProgressDialog pd1;
    private AdView mAdView;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_full_wallpaper);

        //image
        this.url = (String) getIntent().getSerializableExtra("img");
        //image
        this.thumb = (String) getIntent().getSerializableExtra("cover");
        //Artist name
        this.name = (String) getIntent().getSerializableExtra("artistName");
        //Premium status
        this.premium = (boolean) getIntent().getSerializableExtra("premium");

        //load RewardedAd
        initiRewardedVideo();
        //load Inter ads
        initInterAds();

        ig = "";
        fb = "";

        // instance progressdialog
        pd1 = new ProgressDialog(SetFullWallpaper.this);
        pd1.setMessage("loading..");

        tinydb = new TinyDB(getApplicationContext());
        fav = findViewById(R.id.fav);

        // finding Items by id
        setWallpaper = findViewById(R.id.button);
        Download = findViewById(R.id.download);
        share = findViewById(R.id.share);
        unlock = findViewById(R.id.unlock);
        CardView card_view = findViewById(R.id.card_view);
        final ImageView fullImage = findViewById(R.id.fullWallpaper);
        final ImageView blurredBg = findViewById(R.id.blurredBg);

        ImageView back = findViewById(R.id.back);
        TextView artistName = findViewById(R.id.Artist);

        if (!this.name.isEmpty()) {
            artistName.setText("@" + this.name);
        } else {
            artistName.setVisibility(View.GONE);
        }


        //banner ads
        mAdView = findViewById(R.id.adViewFull);
        AdView adView = new AdView(this);
        adView.setAdUnitId(String.valueOf(R.string.banner_anime_wallpaper));
        AdRequest adRequest2 = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest2);

        //PREMIUM
        if (this.premium) {
            unlock.setVisibility(View.VISIBLE);
            setWallpaper.setEnabled(false);
            Download.setEnabled(false);
            share.setEnabled(false);

        } else {
            unlock.setVisibility(View.GONE);
        }

        Glide.with(getApplicationContext())
                .load(thumb)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .priority(Priority.HIGH)
                .into(fullImage);

        Glide.with(getApplicationContext())
                .asBitmap()
                .load(thumb)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        init(blurredBg, resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });

        /*fav button*/
        if (alreadyExist(url, tinydb)) {
            fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_remove));
            clicked = true;
        } else {
            fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_add));
            clicked = false;
        }

        fav.setOnClickListener(view -> {
            if (!clicked) {
                WriteInShared(url, thumb, premium, fb, ig, name, tinydb);
                clicked = true;
                fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_remove));
            } else {
                removeFromArrayList(url, tinydb);
                clicked = false;
                fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_add));
            }
        });

        setWallpaper.setOnClickListener(view -> {
            // Load ADS
            mInterstitialAd.loadAd(adRequest);
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
            });
            if (isMarshmellowAndAbove()) {
                if (checkPermission()) {
                    setWallpaperBoth();
                } else {
                    requestPermission(CODE_PERMISSION_SET);
                }
            } else {
                setWallpaperBoth();
            }
        });

        card_view.setOnClickListener(view -> {
            // Load ADS
            mInterstitialAd.loadAd(adRequest);
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
            });

            Intent FullIntent = new Intent(SetFullWallpaper.this, FullWidth.class);
            FullIntent.putExtra("imgToFull", url);
            startActivity(FullIntent);
        });


        share.setOnClickListener(view -> {
            mInterstitialAd.loadAd(adRequest);
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
            });

            if (isMarshmellowAndAbove()) {
                if (checkPermission()) {
                    shareWallpaper();
                } else {
                    requestPermission(CODE_PERMISSION_SHARE);
                }
            } else {
                shareWallpaper();
            }
        });


        Download.setOnClickListener(v -> {
            if (isMarshmellowAndAbove()) {
                if (checkPermission()) {
                    saveWallpaper();
                } else {
                    requestPermission(CODE_PERMISSION_SAVE);
                }
            } else {
                saveWallpaper();
            }

        });

        back.setOnClickListener(view -> {
            finish();
        });

        unlock.setOnClickListener(view -> {
            if (!pd1.isShowing() && !mRewardedVideoAd.isLoaded()) { // not showing
                mRewardedVideoAd.loadAd("ca-app-pub-3888108786158580/1407706821", new AdRequest.Builder().build());
                pd1.show();
                a = 1;
            } else if (mRewardedVideoAd.isLoaded()) {
                a = 1;
                pd1.dismiss();
                mRewardedVideoAd.show();
            }
        });


    }// End of onCreate


    @Override
    public void onRewardedVideoAdLoaded() {
        if (pd1.isShowing()) {
            pd1.dismiss();
        }
        if (a == 1) {
            mRewardedVideoAd.show();
        }
    }


    @Override
    public void onRewardedVideoAdOpened() {
    }

    @Override
    public void onRewardedVideoStarted() {
    }

    @Override
    public void onRewardedVideoAdClosed() {
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        setWallpaper.setEnabled(true);
        Download.setEnabled(true);
        share.setEnabled(true);
        unlock.setVisibility(View.GONE);
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
    }

    public boolean isMarshmellowAndAbove() {
        return (Build.VERSION.SDK_INT >= 23);
    }


    @SuppressLint("SetTextI18n")
    public void setWallpaperBoth() {
        Dialog myDialogWallpaperBoth = new Dialog(SetFullWallpaper.this);
        myDialogWallpaperBoth.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assert myDialogWallpaperBoth.getWindow() != null;
        myDialogWallpaperBoth.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialogWallpaperBoth.setCancelable(false);
        myDialogWallpaperBoth.setContentView(R.layout.setasforandroidn);
        Button Home = myDialogWallpaperBoth.findViewById(R.id.homescreenbtn);
        Button Parallax = myDialogWallpaperBoth.findViewById(R.id.parallax);
        //TextView title = myDialogWallpaperBoth.findViewById(R.id.texttitle);
        LinearLayout settingParallax = myDialogWallpaperBoth.findViewById(R.id.parallaxsetting);
        //title.setText("Set Wallpaper  : ");
        Home.setText("NORMALLY");
        Parallax.setText("WITH PARALLAX EFFECT");

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null)
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if (sensor == null) {
            Parallax.setVisibility(View.GONE);
            View view = myDialogWallpaperBoth.findViewById(R.id.view);
            view.setVisibility(View.GONE);
            settingParallax.setVisibility(View.GONE);
        }
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = preferences.edit();
        SeekBar seekBarRange = myDialogWallpaperBoth.findViewById(R.id.seekBarRange);
        seekBarRange.setProgress(preferences.getInt("range", 18));
        seekBarRange.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    editor.putInt("range", progress);
                    editor.apply();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        SeekBar seekBarDelay = myDialogWallpaperBoth.findViewById(R.id.seekBarDelay);
        seekBarDelay.setProgress(preferences.getInt("delay", 10));
        seekBarDelay.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    editor.putInt("delay", progress);
                    editor.apply();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        Parallax.setOnClickListener(view -> {
            myDialogWallpaperBoth.dismiss();
            Glide.with(getApplicationContext()).asBitmap()
                    .load(url)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)).into(new CustomTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    if (!isFinishing())
                        new saveImageParallax().execute(resource);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {
                }
            });
        });


        Home.setOnClickListener(v -> {
            myDialogWallpaperBoth.dismiss();
            Glide.with(getApplicationContext()).asBitmap()
                    .load(url)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)).into(new CustomTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    if (!isFinishing())
                        new saveImageforsetaswallpaper().execute(resource);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {

                }
            });
        });
        if (!SetFullWallpaper.this.isFinishing())
            myDialogWallpaperBoth.show();
        myDialogWallpaperBoth.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });
    }


    public void shareWallpaper() {
        Glide.with(getApplicationContext()).asBitmap()
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (!isFinishing())
                            new shareCropped().execute(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });
    }


    private void galleryAddPic(String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(SetFullWallpaper.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(int CODE_PERMISSION) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(SetFullWallpaper.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(SetFullWallpaper.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(SetFullWallpaper.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_PERMISSION);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CODE_PERMISSION_SAVE:
                saveWallpaper();
                break;
            case CODE_PERMISSION_SET:
                setWallpaperBoth();
                break;
            case CODE_PERMISSION_SHARE:
                shareWallpaper();
                break;
        }
    }

    private void init(ImageView image, Bitmap bitmap) {
        Blurry.with(SetFullWallpaper.this)
                .sampling(4)
                .from(bitmap)
                .into(image);
    }

    private void initiRewardedVideo() {
        // Rewarded Video ADs
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        mRewardedVideoAd.loadAd("ca-app-pub-3888108786158580/1407706821", new AdRequest.Builder().build());

    }

    private void initInterAds() {
        //Ads inter
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.inter_anime_wallpaper));
        mInterstitialAd.loadAd(adRequest);
    }


    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    public void WriteInShared(String url, String thumb, boolean premium, String fb, String ig, String name, TinyDB tinydb) {
        tinydb = new TinyDB(getApplicationContext());
        ArrayList<Post> postObjects = tinydb.getListObject("fav", Post.class);
        ArrayList<Post> myfav = new ArrayList<Post>();
        for (Object objs : postObjects) {
            myfav.add((Post) objs);
        }
        Post post = new Post();
        post.setImgUrl(url);
        post.setName(name);
        post.setCat_cover_medium(thumb);
        post.setFacebook(fb);
        post.setInstagram(ig);
        post.setPremium(premium);

        myfav.add(post);
        tinydb.putListObject("fav", myfav);
        Toast.makeText(getApplicationContext(), "Added to Favorite", Toast.LENGTH_SHORT).show();
    }

    public void removeFromArrayList(String url, TinyDB tinydb) {
        ArrayList<Post> myfavSaved;
        myfavSaved = tinydb.getListObject("fav", Post.class);
        for (int i = 0; i < myfavSaved.size(); i++) {
            if (myfavSaved.get(i).getImgUrl().equalsIgnoreCase(url)) {
                myfavSaved.remove(i);
                saveFav(myfavSaved, tinydb);
            }
        }
        Toast.makeText(getApplicationContext(), "Removed from  Favorite", Toast.LENGTH_SHORT).show();
    }

    public void saveFav(ArrayList<Post> arrayList, TinyDB tinydb) {
        tinydb.putListObject("fav", arrayList);
    }

    public boolean alreadyExist(String url, TinyDB tinydb) {
        ArrayList<Post> myfavSaved;
        myfavSaved = tinydb.getListObject("fav", Post.class);
        for (int i = 0; i < myfavSaved.size(); i++) {
            if (myfavSaved.get(i).getImgUrl().equalsIgnoreCase(url)) {
                return true;
            }
        }
        return false;
    }

    public void saveWallpaper() {
        Glide.with(getApplicationContext()).asBitmap()
                .load(url)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)).into(new CustomTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                if (!isFinishing())
                    new saveImage().execute(resource);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {

            }
        });
    }


    @SuppressLint("StaticFieldLeak")
    public class saveImageParallax extends AsyncTask<Bitmap, Void, String> {

        ProgressDialog pd;

        @SuppressLint({"CheckResult", "SetTextI18n"})
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(SetFullWallpaper.this);
            pd.setTitle("Please wait..");
            pd.setIcon(R.mipmap.ic_launcher);
            if (!isFinishing())
                pd.show();
            pd.setOnKeyListener((dialog, keyCode, event) -> {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.cancel();
                    return true;
                }
                return false;
            });
        }


        @Override
        protected String doInBackground(Bitmap... params) {
            Bitmap image = params[0];
            String savedImagePath = null;
            String imageFileName = "ra.png";
            File storageDir = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                            + "/realanime_wallpapers");
            boolean success = true;
            if (!storageDir.exists()) {
                success = storageDir.mkdirs();
            }
            if (success) {
                File imageFile = new File(storageDir, imageFileName);
                savedImagePath = imageFile.getAbsolutePath();
                try {
                    OutputStream fOut = new FileOutputStream(imageFile);
                    image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                galleryAddPic(savedImagePath);
                pd.dismiss();
            }
            return savedImagePath;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            final SharedPreferences sPreference = getSharedPreferences(
                    "pref_key", MODE_PRIVATE);
            final SharedPreferences.Editor spEditor = sPreference.edit();
            spEditor.putString("img_path", s);
            spEditor.apply();
            WallpaperManager wpm = WallpaperManager.getInstance(getApplicationContext());
            WallpaperInfo info = wpm.getWallpaperInfo();
            if (info != null && info.getPackageName().equals(getApplicationContext().getPackageName())) {
                WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
                try {
                    wallpaperManager.clear();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent("android.service.wallpaper.CHANGE_LIVE_WALLPAPER");
                intent.putExtra("android.service.wallpaper.extra.LIVE_WALLPAPER_COMPONENT", new ComponentName(getApplicationContext(), LiveWallpaperService.class));
                try {
                    startActivityForResult(intent, SetFullWallpaper.SET_LIVE_WALL_REQUEST);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "Sorry your device does not support Live Wallpaper", Toast.LENGTH_SHORT).show();
                }
            } else {
                Intent intent = new Intent("android.service.wallpaper.CHANGE_LIVE_WALLPAPER");
                intent.putExtra("android.service.wallpaper.extra.LIVE_WALLPAPER_COMPONENT", new ComponentName(getApplicationContext(), LiveWallpaperService.class));
                try {
                    startActivityForResult(intent, SetFullWallpaper.SET_LIVE_WALL_REQUEST);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "Sorry your device does not support Live Wallpaper", Toast.LENGTH_SHORT).show();
                }
            }
            pd.dismiss();
        }
    }


    @SuppressLint("StaticFieldLeak")
    public class saveImage extends AsyncTask<Bitmap, Void, String> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(SetFullWallpaper.this);
            pd.setTitle("Please wait..");
            pd.setIcon(R.mipmap.ic_launcher);
            pd.setMessage("Downloading");
            pd.show();
        }

        @Override
        protected String doInBackground(Bitmap... params) {
            Bitmap image = params[0];
            String savedImagePath = null;
            String imageFileName = "walp" + System.currentTimeMillis() + ".png";
            File storageDir = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                            + "/RealAnime wallpapers");
            boolean success = true;
            if (!storageDir.exists()) {
                success = storageDir.mkdirs();
            }
            if (success) {
                File imageFile = new File(storageDir, imageFileName);
                savedImagePath = imageFile.getAbsolutePath();
                try {
                    OutputStream fOut = new FileOutputStream(imageFile);
                    image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                galleryAddPic(savedImagePath);
                if (pd.isShowing()) {
                    pd.dismiss();
                }
            }
            return savedImagePath;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(getApplicationContext(), "Picture saved ", Toast.LENGTH_SHORT).show();
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class sharewallpaper extends AsyncTask<Bitmap, Void, String> {

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(SetFullWallpaper.this);
            pd.setTitle("please wait");
            pd.setIcon(R.drawable.ic_share);
            pd.setMessage("setting as wallpaper..");
            pd.setCancelable(false);

            if (Build.VERSION.SDK_INT >= 26) {
                pd.show();
            } else if (Build.VERSION.SDK_INT > 19) {
                pd.show();
            } else {
                pd.show();
            }
        }

        @Override
        protected String doInBackground(Bitmap... params) {
            Bitmap image = params[0];
            // Save this bitmap to a file.
            File cache = getApplicationContext().getExternalCacheDir();
            File sharefile = new File(cache, "toshare.png");
            try {
                FileOutputStream out = new FileOutputStream(sharefile);
                image.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
            } catch (IOException e) {

            }
            // Now send it out to share
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + sharefile));
            try {
                startActivity(Intent.createChooser(share, "Share photo"));
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (Build.VERSION.SDK_INT >= 26) {
                if (pd.isShowing()) {
                    pd.dismiss();
                }
            } else if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class saveImageforsetaswallpaper extends AsyncTask<Bitmap, Void, String> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(SetFullWallpaper.this);
            pd.setIcon(R.drawable.ic_share);
            pd.setMessage("setting as wallpaper..");
            pd.setCancelable(false);

            if (Build.VERSION.SDK_INT >= 26) {
                pd.show();
            } else if (Build.VERSION.SDK_INT > 19) {
                pd.show();
            } else {
                pd.show();
            }
        }

        @Override
        protected String doInBackground(Bitmap... params) {
            final Bitmap image = params[0];
            String savedImagePath = null;
            String imageFileName = "tmp.png";
            File imageFile = null;
            File storageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "realanime_wallpapers");
            boolean success = true;
            if (!storageDir.exists()) {
                success = storageDir.mkdirs();
            }
            if (success) {
                imageFile = new File(storageDir, imageFileName);
                savedImagePath = imageFile.getAbsolutePath();
                try {
                    OutputStream fOut = new FileOutputStream(imageFile);
                    image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                galleryAddPic(savedImagePath);
            }
            Intent intent = new Intent(Intent.ACTION_ATTACH_DATA);
            MimeTypeMap map = MimeTypeMap.getSingleton();
            assert savedImagePath != null;
            String ext = savedImagePath.substring(savedImagePath.lastIndexOf('.') + 1);
            String mimeType = map.getMimeTypeFromExtension(ext);
            Uri uri;
            if (Build.VERSION.SDK_INT > 23) {
                uri = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".fileprovider", imageFile);
            } else {
                uri = Uri.fromFile(imageFile);
            }
            intent.setDataAndType(uri, mimeType);
            intent.putExtra("mimeType", mimeType);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivity(Intent.createChooser(intent, "Set As :"));
            return savedImagePath;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class shareCropped extends AsyncTask<Bitmap, Void, String> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(SetFullWallpaper.this);
            pd.setIcon(R.drawable.ic_share);
            pd.setMessage("Share this picture..");
            pd.setCancelable(false);

            if (Build.VERSION.SDK_INT >= 26) {
                pd.show();
            } else if (Build.VERSION.SDK_INT > 19) {
                pd.show();
            } else {
                pd.show();
            }
        }

        @Override
        protected String doInBackground(Bitmap... params) {
            Bitmap image = params[0];
            File cache = getApplicationContext().getExternalCacheDir();
            File shareFile = new File(cache, "toshare.png");
            try {
                FileOutputStream out = new FileOutputStream(shareFile);
                image.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
            } catch (IOException ignored) {

            }
            if (Build.VERSION.SDK_INT >= 26) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.putExtra("android.intent.extra.STREAM", Uri.parse(MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(), image, "toshare", "share")));
                intent.setType("image/*");
                try {
                    startActivity(Intent.createChooser(intent, "Share photo"));
                } catch (Exception ignored) {
                }
            } else {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/*");
                share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + shareFile));
                try {
                    startActivity(Intent.createChooser(share, "Share photo"));
                } catch (Exception ignored) {

                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(getApplicationContext(), "Go to share it !", Toast.LENGTH_SHORT).show();
            pd.dismiss();
            mInterstitialAd.loadAd(adRequest);
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
            });
        }
    }

}