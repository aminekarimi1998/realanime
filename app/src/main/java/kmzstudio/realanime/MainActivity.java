package kmzstudio.realanime;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.view.MaterialIntroView;
import kmzstudio.realanime.Adapters.About;
import kmzstudio.realanime.Adapters.DataStatus;
import kmzstudio.realanime.Adapters.RecyclerViewClickListener;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, RewardedVideoAdListener {

    public static final String PREFS_NAME = "MyPrefsFile1";
    private static final String URL_DATA = BuildConfig.bsg;
    final AdRequest adRequest = new AdRequest.Builder().build();
    InterstitialAd mInterstitialAd;
    RewardedVideoAd mRewardedVideoAd;
    private RecyclerView recyclerView;
    private Dialog myDialog;
    private MyAdapter adapter;
    private Button rate, dismiss;
    private TextView mTitle;
    private ProgressBar loading;
    private RelativeLayout changeCatLayout;
    private LinearLayout noConnectionField;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.customToolBar);
        setSupportActionBar(toolbar);
        //setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                MainActivity.this, drawer,
                toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        loading = findViewById(R.id.loading);
        noConnectionField = findViewById(R.id.no_oconnection_field);
        ImageView send_button = findViewById(R.id.send_button);
        changeCatLayout = findViewById(R.id.changeCatLayout);

        recyclerView = findViewById(R.id.rc);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        String selectedCategorie = (String) getIntent().getSerializableExtra("selectedCat");

        //init Mobile ads
        MobileAds.initialize(this, getString(R.string.app_id_ads));

        findViewById(R.id.favorit).setVisibility(View.GONE);

        mTitle = findViewById(R.id.title1);

        assert selectedCategorie != null;
        switch (selectedCategorie) {
            case "recent":
                checkInternet(null);
                send_button.setVisibility(View.GONE);
                titleBar(getString(R.string.toolbar_home_title));
                break;

            case "fanarts":
                checkInternet(selectedCategorie);

                new MaterialIntroView.Builder(this)
                        //.enableDotAnimation(true)
                        .enableIcon(false)
                        .setTargetPadding(50)
                        .setFocusGravity(FocusGravity.CENTER)
                        .setFocusType(Focus.NORMAL)
                        .setDelayMillis(500)
                        .enableFadeAnimation(true)
                        .performClick(false)
                        .setInfoText("Noticed That ? Yes we added this categorie to put your ART here, " +
                                "so click this button if you want to send us your work, attach your image with you signature 'name'" +
                                " and it will be soon published!\n" +
                                " ARIGATO GOZAIMASUU")
                        .setTextColor(Color.WHITE)
                        .setInfoTextSize(18)
                        .setTarget(send_button)
                        .performClick(true)
                        .setUsageId("intro_card")
                        .show();

                send_button.setVisibility(View.VISIBLE);
                break;

            case "manga":
            case "anime":
            case "glitchanime":
                send_button.setVisibility(View.INVISIBLE);
                checkInternet(selectedCategorie);
                break;
        }


        //banner ads
        mAdView = findViewById(R.id.adView);
        AdView adView = new AdView(this);
        adView.setAdUnitId(String.valueOf(R.string.banner_anime_wallpaper));

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        //Check Phone Orientation : Vertical | Horizontal
        checkOrientation();


        int PackageNameNumber = getApplicationContext().getPackageName().length();
        int nawawi = 9;
        if (PackageNameNumber != (nawawi * 2) + 1) {
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
        }

        mTitle = findViewById(R.id.title1);
        Button refresh = findViewById(R.id.refresh);

        //Create Title of ToolBar
        //titleBar("漫画 REALANIME");


        //3awedni f banner ok ?
        //TODO uncomment on release
       /* if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        }*/

        refresh.setOnClickListener(view -> {
            if (!checkInternet(null)) {
                Toast.makeText(MainActivity.this, "Please activate your internet ",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        /*Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if (!isFinishing()) {
                    users();
                }
            }
        }, 4000);*/


        send_button.setOnClickListener(v -> {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "realanimeartworks@gmail.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "ARTWORK RealAnime");
            try {
                startActivity(Intent.createChooser(emailIntent, "Sending..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(MainActivity.this, "There are no email account", Toast.LENGTH_SHORT).show();
            }
        });

    } // onCreate

    private void users() {
        /* Init dialog  */
        myDialog = new Dialog(MainActivity.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assert myDialog.getWindow() != null;
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCancelable(true);
        myDialog.setContentView(R.layout.rate_us_dialog);
        rate = myDialog.findViewById(R.id.rateButton);
        dismiss = myDialog.findViewById(R.id.hideButton);
        TextView titleRateUs = myDialog.findViewById(R.id.TitleRate);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String skipMessage = settings.getString("skipMessage", "NOT checked");

        rate.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName()));
            startActivity(browserIntent);
            myDialog.dismiss();
        });
        dismiss.setOnClickListener(view -> {
            SharedPreferences settings1 = getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings1.edit();
            editor.putString("skipMessage", "checked");
            editor.apply();
            myDialog.dismiss();
        });
        if (!skipMessage.equals("checked")) {
            myDialog.show();
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void titleBar(String newTitle) {
        mTitle.setText(newTitle);
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }

    private boolean checkInternet(String activity) {
        if (recyclerView.getLayoutManager() != null)
            recyclerView.getLayoutManager().scrollToPosition(0);
        if (!isNetworkConnected()) {
            changeCatLayout.setVisibility(View.INVISIBLE);
            noConnectionField.setVisibility(View.VISIBLE);
            return false;
        } else {
            changeCatLayout.setVisibility(View.INVISIBLE);
            //Bring Data :
            changeCatLayout.setVisibility(View.VISIBLE);
            noConnectionField.setVisibility(View.INVISIBLE);
            refreshContent(activity);
        }
        return true;
    }

    private void checkOrientation() {

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        } else {
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        }
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        mTitle = findViewById(R.id.title1);

        switch (id) {

            case R.id.recent:
                finish();
                break;
            case R.id.removeAds:
                View contextView = findViewById(R.id.nav_view);
                Snackbar.make(contextView, R.string.remove_ads, BaseTransientBottomBar.LENGTH_LONG).show();
                break;
            case R.id.favorit:
                Intent favorit = new Intent(MainActivity.this, FavoriteActivity.class);
                startActivity(favorit);
                break;
            case R.id.aboutus:
                Intent about_page = new Intent(MainActivity.this, About.class);
                startActivity(about_page);
                break;
            case R.id.privacypolicy:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://stylesh.000webhostapp.com/ppapps.html"));
                startActivity(browserIntent);
                break;

            case R.id.actionshare:
                try {
                    Intent it = new Intent(Intent.ACTION_SEND);
                    it.setType("text/plain");
                    it.putExtra(Intent.EXTRA_SUBJECT, "Real Anime Wallpapers");
                    String sAux = "\nWhat if animes was in real life ? Open your Manga Mind with us\n\n";
                    sAux = sAux + "https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName() + "\n\n";
                    it.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(it, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void refreshContent(@Nullable String queryParameter) {

        fetchRemoteData(queryParameter, new DataStatus() {
            @Override
            public void onSuccess(List<Post> posts) {
                changeCatLayout.setVisibility(View.INVISIBLE);
                if (adapter == null) {
                    adapter = new MyAdapter(posts, MainActivity.this, new RecyclerViewClickListener() {
                        @Override
                        public void onClick(View view, Post post) {
                            Intent intent = new Intent(MainActivity.this, SetFullWallpaper.class);
                            intent.putExtra("img", post.getImgUrl());
                            intent.putExtra("cover", post.getCat_cover_medium());
                            intent.putExtra("artistName", post.getName());
                            intent.putExtra("premium", post.getPremium());
                            //intent.putExtra("facebook", post.getFacebook());
                            //intent.putExtra("ig", post.getInstagram());
                            startActivity(intent);
                        }
                    });
                    recyclerView.setAdapter(adapter);

                } else {
                    adapter.getItems().clear();
                    adapter.getItems().addAll(posts);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(Exception e) {
                //Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void fetchRemoteData(String queryParameter, final DataStatus callback) {

        String queryUrl = TextUtils.isEmpty(queryParameter) ?
                URL_DATA.replace("*", "")
                : URL_DATA.replace("*", "-" + queryParameter);

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                queryUrl,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String s) {

                        List<Post> listItems = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            JSONArray array = jsonObject.getJSONArray("bgs");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                Post item = new Post(
                                        object.getBoolean("new"),
                                        object.getString("img"),
                                        object.getString("img"),
                                        object.getBoolean("premium"),
                                        object.getString("facebook"),
                                        object.getString("ig"),
                                        object.getString("name")
                                );

                                listItems.add(item);

                            }
                            callback.onSuccess(listItems);

                        } catch (JSONException e) {
                            // to custom
                            // callback.onError(e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "There is an error please try later !", Toast.LENGTH_LONG).show();
                        loading.setVisibility(View.INVISIBLE);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }


    @Override
    public void onRewardedVideoAdLoaded() {
        mRewardedVideoAd.show();
    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {

        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {

    }

    private void initiRewardedVideo() {
        // Rewarded Video ADs
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        mRewardedVideoAd.loadAd("ca-app-pub-3888108786158580/1407706821", new AdRequest.Builder().build());
    }

    private void initInterAds() {
        //Ads inter
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.inter_anime_wallpaper));
        mInterstitialAd.loadAd(adRequest);
    }
}



