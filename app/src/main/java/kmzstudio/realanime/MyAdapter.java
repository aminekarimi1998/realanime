package kmzstudio.realanime;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import kmzstudio.realanime.Adapters.RecyclerViewClickListener;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<Post> posts;
    private Context context;
    private RecyclerViewClickListener mClickListener;

    public MyAdapter(List<Post> posts, Context context, RecyclerViewClickListener clickListener) {
        this.posts = posts;
        this.context = context;
        this.mClickListener = clickListener;
    }

    public List<Post> getItems() {
        return posts;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout,
                parent, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindModel(posts.get(position));
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Post mPost;
        ImageView image, cover;
        CardView newTag;


        ViewHolder(View v) {
            super(v);
            image = itemView.findViewById(R.id.imageView);
            cover = itemView.findViewById(R.id.premiumCover);
            newTag = itemView.findViewById(R.id.newTag);

            v.setOnClickListener(this);
        }

        @SuppressLint("SetTextI18n")
        void bindModel(Post post) {

            this.mPost = post;

            Glide
                    .with(context)
                    .load(mPost.getCat_cover_medium())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .into(image);

            if (mPost.isNewAtt()){
                newTag.setVisibility(View.VISIBLE);
            }else{
                newTag.setVisibility(View.GONE);
            }

            if (post.getPremium()) {
                //premium.setVisibility(View.VISIBLE);
                cover.setVisibility(View.VISIBLE);
            } else {
                //premium.setVisibility(View.GONE);
                cover.setVisibility(View.GONE);
            }


        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, mPost);
        }
    }

}
