package kmzstudio.realanime.Artist;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by karimi amine on 3/12/2018.
 */

public interface ScrollStateChangeListener<T extends RecyclerView.ViewHolder> {

    void onScrollStart(T currentItemHolder, int adapterPosition); //called when scroll is started, including programatically initiated scroll

    void onScrollEnd(T currentItemHolder, int adapterPosition); //called when scroll ends
    /**
     * Called when scroll is in progress.
     * @param scrollPosition is a value inside the interval [-1f..1f], it corresponds to the position of currentlySelectedView.
     * In idle state:
     * |view1|  |currentlySelectedView|  |view2|
     * -view1 is on position -1;
     * -currentlySelectedView is on position 0;
     * -view2 is on position 1.
     * @param currentIndex - index of current view
     * @param newIndex - index of a view which is becoming the new current
     * @param currentHolder - ViewHolder of a current view
     * @param newCurrentHolder - ViewHolder of a view which is becoming the new current
     */
    void onScroll(float scrollPosition, int currentIndex, int newIndex, @Nullable T currentHolder, @Nullable T newCurrentHolder);
}