package kmzstudio.realanime.Artist;

import java.util.List;

import kmzstudio.realanime.Post;

public interface ArtistDataStatus {
    void onSuccess(List<ArtistPost> artists);
    void onError(Exception e);
}
