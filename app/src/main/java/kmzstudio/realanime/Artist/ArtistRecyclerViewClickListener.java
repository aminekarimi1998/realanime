package kmzstudio.realanime.Artist;

import android.view.View;

import kmzstudio.realanime.Post;


/**
 * Created by yousra on 04/10/2017.
 */

public interface ArtistRecyclerViewClickListener {
    public void onClick(View view, ArtistPost artist);
}
