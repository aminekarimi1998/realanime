package kmzstudio.realanime.Artist;

import android.content.Intent;
import android.content.res.Configuration;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kmzstudio.realanime.Adapters.DataStatus;
import kmzstudio.realanime.Adapters.RecyclerViewClickListener;
import kmzstudio.realanime.MyAdapter;
import kmzstudio.realanime.Post;
import kmzstudio.realanime.R;
import kmzstudio.realanime.SetFullWallpaper;


public class ListArtistWalls extends AppCompatActivity {

    private static final String URL_DATA = "https://dk3te.app.goo.gl/walls";
    private RecyclerView artistWallsRecycler;
    private MyAdapter adapter;
    String artistName;
    Toolbar myToolbar;
    private AdView mAdView;
    ProgressBar progressBarList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_artist_walls);
        myToolbar = findViewById(R.id.customToolBar);
        setSupportActionBar(myToolbar);

        progressBarList = findViewById(R.id.progressBarList);

        artistWallsRecycler = findViewById(R.id.artistWallsRecycler);
        artistWallsRecycler.setHasFixedSize(true);
        artistWallsRecycler.setLayoutManager(new LinearLayoutManager(this));

        //banner ads
        mAdView = findViewById(R.id.adViewArtist);
        AdView adView = new AdView(this);
        adView.setAdUnitId(String.valueOf(R.string.banner_artist));
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        this.artistName = (String) getIntent().getSerializableExtra("artistName");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(artistName);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        refreshContent();
        backButton();
        checkOrientation();
    }

    private void refreshContent() {
        fetchRemoteData(new DataStatus() {
            @Override
            public void onSuccess(List<Post> posts) {
                progressBarList.setVisibility(View.GONE);
                if (adapter == null) {
                    adapter = new MyAdapter(posts, ListArtistWalls.this, (view, post) -> {
                        Intent intent = new Intent(ListArtistWalls.this, SetFullWallpaper.class);
                        intent.putExtra("img", post.getImgUrl());
                        intent.putExtra("cover", post.getCat_cover_medium());
                        intent.putExtra("artistName", post.getName());
                        intent.putExtra("premium", post.getPremium());
                        intent.putExtra("facebook", post.getFacebook());
                        intent.putExtra("ig", post.getInstagram());
                        startActivity(intent);
                    });
                    artistWallsRecycler.setAdapter(adapter);
                } else {
                    adapter.getItems().clear();
                    adapter.getItems().addAll(posts);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(Exception e) {
                //Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        return super.onSupportNavigateUp();
    }

    private void backButton() {
        myToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black));
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void fetchRemoteData(final DataStatus callback) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URL_DATA,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String s) {

                        List<Post> listItems = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            JSONArray array = jsonObject.getJSONArray("bgs");
                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.getJSONObject(i);
                                if (object.getString("name").equals(artistName)){

                                    Post item = new Post(
                                            object.getBoolean("new"),
                                            object.getString("img"),
                                            object.getString("img"),
                                            object.getBoolean("premium"),
                                            object.getString("facebook"),
                                            object.getString("ig"),
                                            object.getString("name")
                                    );

                                    listItems.add(item);
                                }

                            }
                            callback.onSuccess(listItems);

                        } catch (JSONException e) {
                            // to custom
                            // callback.onError(e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ListArtistWalls.this, "There is an error please try later !", Toast.LENGTH_LONG).show();
                        //loading.setVisibility(View.INVISIBLE);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void checkOrientation() {

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            artistWallsRecycler.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        }
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }


}
