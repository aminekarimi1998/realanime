package kmzstudio.realanime.Artist;

public class ArtistPost {

    private String profilName,
            profilProfileImage,
            profilCoverImage,
            profilFacebook,
            website,
            profilInstagram,
            img1, img2, img3;


    public ArtistPost(String profilName, String profilProfileImage, String profilFacebook, String profilInstagram) {

        this.profilProfileImage = transform(profilProfileImage);
        this.profilName = profilName;
        this.profilFacebook = profilFacebook;
        this.profilInstagram = profilInstagram;
    }

    public String getProfilName() {
        return profilName;
    }

    public String getProfilProfileImage() {
        return profilProfileImage;
    }

    public String getProfilCoverImage() {
        return profilCoverImage;
    }

    public String getProfilFacebook() {
        return profilFacebook;
    }

    public String getProfilInstagram() {
        return profilInstagram;
    }


    private String transform(String th) {
        if (th.contains(".jpg")) {
            th = th.replace(".jpg", "l.jpg");
        } else if (th.contains(".png")) {
            th = th.replace(".png", "l.png");
        }
        return th;
    }
}
