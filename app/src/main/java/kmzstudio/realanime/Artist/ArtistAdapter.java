package kmzstudio.realanime.Artist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.util.List;
import kmzstudio.realanime.R;


public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ViewHolder> {

    private List<ArtistPost> posts;
    private Context context;
    private ArtistRecyclerViewClickListener mClickListener;

    public ArtistAdapter(List<ArtistPost> posts, Context context, ArtistRecyclerViewClickListener clickListener) {
        this.posts = posts;
        this.context = context;
        this.mClickListener = clickListener;

    }

    public List<ArtistPost> getItems() {
        return posts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.profil_cardview, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindModel(posts.get(position));
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ArtistPost mPost;
        ImageView profileImage;
        TextView nameProfile, fbArtist, igArtist;


        ViewHolder(View v) {
            super(v);
            fbArtist = itemView.findViewById(R.id.fbArtist);
            igArtist = itemView.findViewById(R.id.igArtist);
            profileImage = itemView.findViewById(R.id.profileImage);
            nameProfile = itemView.findViewById(R.id.nameArtist);

            v.setOnClickListener(this);
        }

        @SuppressLint("SetTextI18n")
        void bindModel(ArtistPost post) {

            this.mPost = post;

            nameProfile.setText(mPost.getProfilName());

            fbArtist.setText(mPost.getProfilFacebook());

            igArtist.setText(mPost.getProfilInstagram());

            glideIt(profileImage, mPost.getProfilProfileImage()); /* PDP */


        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, mPost);
        }
    }


    private void glideIt(ImageView image, String url) {

        Glide
                .with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .into(image);

    }
}
