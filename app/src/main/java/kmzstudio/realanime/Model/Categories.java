package kmzstudio.realanime.Model;

public class Categories {
    private String title;
    private String image;

    public Categories(String title, String image) {
        this.title = title;
        String th = image;

        if (th.contains(".jpg")) {
            th = th.replace(".jpg", "l.jpg");
        } else if (th.contains(".png")) {
            th = th.replace(".png", "l.png");
        }
        this.image = th;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }
}
