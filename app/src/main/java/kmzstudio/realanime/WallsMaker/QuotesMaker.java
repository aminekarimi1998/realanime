package kmzstudio.realanime.WallsMaker;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.xiaopo.flying.sticker.BitmapStickerIcon;
import com.xiaopo.flying.sticker.DeleteIconEvent;
import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.FlipHorizontallyEvent;
import com.xiaopo.flying.sticker.Sticker;
import com.xiaopo.flying.sticker.StickerView;
import com.xiaopo.flying.sticker.TextSticker;
import com.xiaopo.flying.sticker.ZoomIconEvent;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.view.MaterialIntroView;
import kmzstudio.realanime.R;
import me.priyesh.chroma.ChromaDialog;
import me.priyesh.chroma.ColorMode;
import me.priyesh.chroma.ColorSelectListener;

public class QuotesMaker extends AppCompatActivity implements StickerBSFragment.StickerListener {
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final String TAG = QuotesMaker.class.getSimpleName();
    public Bitmap bitmapSelected;
    AdRequest adRequest;
    LinearLayout bgButton, Stickers, addText, llapp;
    Button Save;
    ImageView img;
    InterstitialAd mInterstitialAd;
    Dialog myDialog;
    ImageButton gallery, appImagesStock;
    StickerBSFragment mStickerBSFragment;
    Boolean textAdded = false;
    TextView bg_text, txt_text, char_text;
    private int mSelectedColorForTExt;
    private StickerView stickerView;
    private boolean clicked;
    private Typeface typeFaceChooser;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_quotes_maker);

        adRequest = new AdRequest.Builder().build();
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.inter_anime_wallpaper));
        //mInterstitialAd.loadAd(adRequest);

        /* --------- finding views by id ---------*/
        //bg_text = findViewById(R.id.bg_text);
        txt_text = findViewById(R.id.txt_text);
        char_text = findViewById(R.id.char_text);
        //bgButton = findViewById(R.id.bgcolor);
        Stickers = findViewById(R.id.stickers);
        addText = findViewById(R.id.addtext);
        Save = findViewById(R.id.save);
        img = findViewById(R.id.imageview);
        /*--------------------------------------*/

        /*--------------------------------------*/
        /*Dialog background chooser from Gallery*/
        myDialog = new Dialog(QuotesMaker.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assert myDialog.getWindow() != null;
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCancelable(true);
        myDialog.setContentView(R.layout.stickers_source_chooser);
        gallery = myDialog.findViewById(R.id.gallery);
        appImagesStock = myDialog.findViewById(R.id.appImagesStock);
        llapp = myDialog.findViewById(R.id.llapp);
        llapp.setVisibility(View.GONE);
        /*views and buttons*/
        TextView title = myDialog.findViewById(R.id.titledialog);

        TextView gallerytxt = myDialog.findViewById(R.id.gallerytxt);
        TextView apptxt = myDialog.findViewById(R.id.apptxt);
        TextView txt = myDialog.findViewById(R.id.txtstickers);
        /*setting image and text content*/
        title.setText("Background Chooser");
        gallerytxt.setText("From Your Gallery");
        apptxt.setText("From App");
        txt.setText("Select Background");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            gallery.setImageDrawable(getResources().getDrawable(R.drawable.sticker_gallery, getApplicationContext().getTheme()));
            appImagesStock.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher, getApplicationContext().getTheme()));
        } else {
            gallery.setImageDrawable(getResources().getDrawable(R.drawable.sticker_gallery));
            appImagesStock.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
        }
        /*--------------------------------------*/

        Typeface tf1 = Typeface.createFromAsset(getAssets(), "CC Wild Words Roman.ttf");
        //bg_text.setTypeface(tf1);
        txt_text.setTypeface(tf1);
        char_text.setTypeface(tf1);
        Save.setTypeface(tf1);


        /*bgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                llapp.setVisibility(View.GONE);

                gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pickFromGallery(1);
                        myDialog.dismiss();
                    }
                });
                myDialog.show();
            }
        });*/


        stickerView = findViewById(R.id.sticker_view);
        BitmapStickerIcon deleteIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                com.xiaopo.flying.sticker.R.drawable.sticker_ic_close_white_18dp),
                BitmapStickerIcon.LEFT_TOP);
        deleteIcon.setIconEvent(new DeleteIconEvent());

        BitmapStickerIcon zoomIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                com.xiaopo.flying.sticker.R.drawable.sticker_ic_scale_white_18dp),
                BitmapStickerIcon.RIGHT_BOTOM);
        zoomIcon.setIconEvent(new ZoomIconEvent());

        BitmapStickerIcon flipIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                com.xiaopo.flying.sticker.R.drawable.sticker_ic_flip_white_18dp),
                BitmapStickerIcon.RIGHT_TOP);
        flipIcon.setIconEvent(new FlipHorizontallyEvent());
        final ArrayList<BitmapStickerIcon> bitmapStickerIcons = new ArrayList<>();
        bitmapStickerIcons.add(deleteIcon);
        bitmapStickerIcons.add(zoomIcon);
        bitmapStickerIcons.add(flipIcon);
        stickerView.setIcons(bitmapStickerIcons);
        stickerView.setLocked(false);
        stickerView.setConstrained(true);
        flipIcon.setIconEvent(new FlipHorizontallyEvent());
        addText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddTextDialog();
            }
        });
        stickerView.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded");
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                if (!clicked) {
                    ArrayList<BitmapStickerIcon> emptyIcons = new ArrayList<>();
                    stickerView.setIcons(emptyIcons);
                    clicked = true;
                } else {
                    stickerView.setIcons(bitmapStickerIcons);
                    clicked = false;
                }
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished");
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onDoubleTapped: double tap will be with two click");
            }
        });

        mStickerBSFragment = new StickerBSFragment();
        mStickerBSFragment.setStickerListener(this);
        Stickers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                llapp.setVisibility(View.VISIBLE);

                gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pickFromGallery(0);
                        myDialog.dismiss();
                    }
                });
                appImagesStock.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mStickerBSFragment.show(getSupportFragmentManager(), mStickerBSFragment.getTag());
                        myDialog.dismiss();
                    }
                });
                myDialog.show();
            }
        });

        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterstitialAd.loadAd(adRequest);
                mInterstitialAd.setAdListener(new AdListener() {
                    public void onAdLoaded() {
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                        }
                    }

                    public void onAdClosed() {

                    }
                });
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        File file = FileUtil.getNewFile(QuotesMaker.this, "Quotes");
                        if (file != null) {
                            stickerView.save(file);
                            Toast.makeText(QuotesMaker.this, "Image Saved In Gallery",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(QuotesMaker.this, "the file is null", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        requestPermission();
                    }
                } else {
                    File file = FileUtil.getNewFile(QuotesMaker.this, "Quotes");
                    if (file != null) {
                        stickerView.save(file);
                        Toast.makeText(QuotesMaker.this, "Image Saved In Gallery",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(QuotesMaker.this, "the file is null", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickFromGallery(1);
                myDialog.dismiss();
            }
        });

        myDialog.show();

        new MaterialIntroView.Builder(this)
                //.enableDotAnimation(true)
                .enableIcon(false)
                .setTargetPadding(50)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.NORMAL)
                .setDelayMillis(500)
                .setIdempotent(true)
                .enableFadeAnimation(true)
                .performClick(false)
                .setInfoText("You can select another background by clicking this button!")
                .setTextColor(Color.WHITE)
                .setInfoTextSize(18)
                .setTarget(bgButton)
                .performClick(true)
                .setUsageId("intro_card")
                .show();

    }

    private void pickFromGallery(int requestCode) {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, requestCode);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(QuotesMaker.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(QuotesMaker.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(QuotesMaker.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(QuotesMaker.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @SuppressLint("CheckResult")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    File file = FileUtil.getNewFile(QuotesMaker.this, "Sticker");
                    if (file != null) {
                        stickerView.save(file);
                        Toast.makeText(QuotesMaker.this, "Image Saved In Gallery",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(QuotesMaker.this, "the file is null", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

    public void testAdd(View view, String text, int Color) {
        final TextSticker sticker = new TextSticker(this);
        sticker.setText(text);
        sticker.setTypeface(typeFaceChooser);
        sticker.setTextColor(Color);
        sticker.setTextAlign(Layout.Alignment.ALIGN_CENTER);
        sticker.resizeText();
        stickerView.addSticker(sticker);


    }

    private void showAddTextDialog() {
        final Dialog myDialog = new Dialog(QuotesMaker.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assert myDialog.getWindow() != null;
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCancelable(true);
        myDialog.setContentView(R.layout.addtextdialog);

        Button ok = myDialog.findViewById(R.id.ok);
        Button color = myDialog.findViewById(R.id.color);
        Button close = myDialog.findViewById(R.id.close);
        Button font = myDialog.findViewById(R.id.font);
        final EditText time = myDialog.findViewById(R.id.editext);

        font.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFontsChooserDialog();
            }
        });
        color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ChromaDialog.Builder()
                        .initialColor(Color.WHITE)
                        .colorMode(ColorMode.RGB)
                        .onColorSelected(new ColorSelectListener() {
                            @Override
                            public void onColorSelected(int color) {
                                mSelectedColorForTExt = color;
                            }
                        })
                        .create()
                        .show(getSupportFragmentManager(), "dialog");
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.dismiss();
                if (time.getText().toString().isEmpty()) {
                    Toast.makeText(QuotesMaker.this, "Field empty", Toast.LENGTH_SHORT).show();
                } else {
                    String TextAfterSpace = time.getText().toString();
                    if (mSelectedColorForTExt == 0) {
                        testAdd(view, TextAfterSpace, Color.BLACK);
                    } else {
                        testAdd(view, TextAfterSpace, mSelectedColorForTExt);
                    }

                }
            }
        });
    }

    private void showFontsChooserDialog() {
        final Dialog myDialog = new Dialog(QuotesMaker.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        assert myDialog.getWindow() != null;
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCancelable(false);
        myDialog.setContentView(R.layout.textfontchooser);
        Button ok = myDialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.dismiss();
            }
        });
        RadioGroup grp = myDialog.findViewById(R.id.grp);

        //*Set RadioButtons Fonts
        RadioButton fullwidth = myDialog.findViewById(R.id.ccwild);
        final Typeface font_fullwidth = Typeface.createFromAsset(getAssets(), "CC Wild Words Roman.ttf");
        fullwidth.setTypeface(font_fullwidth);

        RadioButton retrowave = myDialog.findViewById(R.id.nextart);
        final Typeface nextart = Typeface.createFromAsset(getAssets(), "NEXT ART_SemiBold.otf");
        retrowave.setTypeface(nextart);

        RadioButton lazer = myDialog.findViewById(R.id.rozha);
        final Typeface rozha = Typeface.createFromAsset(getAssets(), "font.ttf");
        lazer.setTypeface(rozha);

        grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                switch (id) {
                    case R.id.ccwild:
                        typeFaceChooser = font_fullwidth;
                        break;

                    case R.id.nextart:
                        typeFaceChooser = nextart;
                        break;

                    case R.id.rozha:
                        typeFaceChooser = rozha;
                        break;
                }
            }
        });
        if (!isFinishing())
            myDialog.show();
        myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });


    }

    private void loadSticker(Bitmap bitmap) {
        Drawable iDrawable = new BitmapDrawable(getResources(), bitmap);
        stickerView.addSticker(new DrawableSticker(iDrawable));
    }

    @Override
    public void onStickerClick(Bitmap bitmap) {
        loadSticker(bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            /*Stickers*/
            case 0:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    try {
                        bitmapSelected = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (bitmapSelected == null) {
                        Toast.makeText(this, "null", Toast.LENGTH_SHORT).show();
                    } else {
                        Drawable iDrawable = new BitmapDrawable(getResources(), bitmapSelected);
                        stickerView.addSticker(new DrawableSticker(iDrawable));
                    }
                } else {
                    Toast.makeText(this, "No image selected", Toast.LENGTH_SHORT).show();
                }
                break;

            /*Background*/
            case 1:
                if (resultCode == RESULT_OK) {

                    Uri selectedImage = data.getData();
                    try {
                        bitmapSelected = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (bitmapSelected == null) {
                        Toast.makeText(this, "null", Toast.LENGTH_SHORT).show();
                    } else {
                        //Drawable iDrawable = new BitmapDrawable(getResources(), bitmapSelected);
                        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        img.setImageBitmap(bitmapSelected);

                    }
                } else {
                    Toast.makeText(this, "No image selected", Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

}