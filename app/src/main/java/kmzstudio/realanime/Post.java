package kmzstudio.realanime;

public class Post {

    private String name, imgUrl, cat_cover_medium, facebook, instagram;
    private boolean premium, newAtt;

    public Post(boolean newAtt, String imgUrl, String cat_cover_medium, boolean premium, String facebook, String instagram, String name) {

        String th = cat_cover_medium;
        if (th.contains(".jpg")) {
            th = th.replace(".jpg", "l.jpg");
        } else if (th.contains(".png")) {
            th = th.replace(".png", "l.png");
        }
        this.cat_cover_medium = th;
        this.imgUrl = imgUrl;
        this.newAtt = newAtt;
        this.name = name;
        this.premium = premium;
        this.facebook = facebook;
        this.instagram = instagram;
    }

    public Post() {
    }

    public Post(String imgUrl, String cat_cover_medium, boolean premium, String facebook, String instagram, String name) {

        this.cat_cover_medium = cat_cover_medium;
        this.imgUrl = imgUrl;
        this.name = name;
        this.premium = premium;
        this.facebook = facebook;
        this.instagram = instagram;
    }


    Post(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public boolean isNewAtt() {
        return newAtt;
    }

    public void setNewAtt(boolean newAtt) {
        this.newAtt = newAtt;
    }

    public String getCat_cover_medium() {
        return cat_cover_medium;
    }

    public void setCat_cover_medium(String cat_cover_medium) {
        this.cat_cover_medium = cat_cover_medium;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public boolean getPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }
}
